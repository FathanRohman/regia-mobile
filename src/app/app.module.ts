import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LazyLoadImageModule } from "ng-lazyload-image";
import { UniqueDeviceID } from "@ionic-native/unique-device-id";

import { HomePage } from '../pages/home/home';

import { ProspectPage } from '../pages/prospect/prospect';
import { ProspectNewPage } from '../pages/prospect-new/prospect-new';
import { ProspectDetailPage } from '../pages/prospect-detail/prospect-detail';

import { OpportunityPage } from '../pages/opportunity/opportunity';
import { OpportunityNewPage } from '../pages/opportunity-new/opportunity-new';
import { OpportunityDetailPage } from '../pages/opportunity-detail/opportunity-detail';

import { ProductPage } from '../pages/product/product'
import { ProductDetailPage } from '../pages/product-detail/product-detail'
import { ProductDetailUnitPage } from '../pages/product-detail-unit/product-detail-unit'
import { ProductUnitTypeListPage } from '../pages/product-unit-type-list/product-unit-type-list'

import { NpvCalculatorPage } from '../pages/npv-calculator/npv-calculator'

import { LoginPage } from '../pages/login/login';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { GlobalProvider } from '../providers/global/global';
import { ProductProvider } from '../providers/product/product';
import { ProspectProvider } from '../providers/prospect/prospect';
import { OpportunityProvider } from '../providers/opportunity/opportunity';
import { NpvProvider } from '../providers/npv/npv';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { DefaultPage } from '../pages/default/default';
import { MorePage } from '../pages/more/more';
import { PaymentMethodPage } from '../pages/payment-method/payment-method';

@NgModule({
  declarations: [
    MyApp,
    ProspectPage,
    OpportunityPage,
    HomePage,
    LoginPage,
    ProspectNewPage,
    ProspectDetailPage,
    OpportunityNewPage,
    OpportunityDetailPage,
    ProductPage,
    ProductDetailPage,
    ProductDetailUnitPage,
    ProductUnitTypeListPage,
    NpvCalculatorPage,
    DefaultPage,
    MorePage,
    PaymentMethodPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    LazyLoadImageModule,
    IonicModule.forRoot(MyApp, {}, {
      links: [
        { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: HomePage, name: 'HomePage', segment: 'home-page' },
        { component: MorePage, name: 'MorePage,', segment: 'more-page' },
        { component: ProductPage, name: 'ProductPage', segment: 'product-page' },
        { component: ProductDetailPage, name: 'ProductDetailPage', segment: 'product-page/:id' },
        { component: ProductDetailUnitPage, name: 'ProductDetailUnitPage', segment: 'product-detail-unit-page' },
        { component: ProductUnitTypeListPage, name: 'ProductUnitTypeListPage', segment: 'product-unit-type-list-page' },
        { component: ProspectPage, name: 'ProspectPage', segment: 'prospect-page' },
        { component: ProspectNewPage, name: 'ProspectNewPage', segment: 'prospect-new-page' },
        { component: ProspectDetailPage, name: 'ProspectDetailPage', segment: 'prospect-page/:id' },
        { component: OpportunityPage, name: 'OpportunityPage', segment: 'opportunity-page' },
        { component: OpportunityNewPage, name: 'OpportunityNewPage', segment: 'opportunity-new-page' },
        { component: PaymentMethodPage, name: 'PaymentMethodPage', segment: 'payment-method-page' },
        { component: OpportunityDetailPage, name: 'OpportunityDetailPage', segment: 'opportunity-page/:id' },
        { component: NpvCalculatorPage, name: 'NpvCalculatorPage', segment: 'npv-calculator-page' },
        { component: LoginPage, name: 'LoginPage', segment: 'login-page' }
      ]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProspectPage,
    OpportunityPage,
    HomePage,
    LoginPage,
    ProspectNewPage,
    ProspectDetailPage,
    OpportunityNewPage,
    OpportunityDetailPage,
    ProductPage,
    ProductDetailPage,
    ProductDetailUnitPage,
    ProductUnitTypeListPage,
    NpvCalculatorPage,
    TabsPage,
    DefaultPage,
    MorePage,
    PaymentMethodPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UniqueDeviceID,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    ProductProvider,
    ProspectProvider,
    OpportunityProvider,
    NpvProvider,
    AuthenticationProvider
  ]
})
export class AppModule {}
