import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  // rootPage:any = TabsPage;
  rootPage: any = LoginPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    screenOrientation: ScreenOrientation,
    public uniqueDeviceID: UniqueDeviceID
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      statusBar.backgroundColorByHexString("#BC232C");

      // screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);

      this.generateDeviceId()
    });
  }

  generateDeviceId () {
    this.uniqueDeviceID.get()
      .then((uuid: any) => console.log(uuid))
      .catch((error: any) => console.log(error));
  }
}
