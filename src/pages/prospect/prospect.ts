import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProspectNewPage } from '../prospect-new/prospect-new';
import { ProspectDetailPage } from '../prospect-detail/prospect-detail';

@Component({
  selector: 'page-prospect',
  templateUrl: 'prospect.html'
})
export class ProspectPage {

  constructor(public navCtrl: NavController) {

  }

  toNewProspect () {
    this.navCtrl.push(ProspectNewPage, {}, { animate: true });
  }

  detailProspect () {
    this.navCtrl.push(ProspectDetailPage)
  }

  toSearchPropspect () {
    console.log('Search Prospect')
  }
}
