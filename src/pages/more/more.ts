import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController } from 'ionic-angular';
import { NpvCalculatorPage } from '../npv-calculator/npv-calculator';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public app: App
  ) {
  }

  ionViewDidLoad() {}

  goNpvCalculator () {
    this.navCtrl.push(NpvCalculatorPage)
  }

  presentConfirmLogout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm exit App',
      message: 'Do you want to sign out this app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.app.getRootNav().setRoot(LoginPage);
            localStorage.clear()
          }
        }
      ]
    });
    alert.present();
  }
}
