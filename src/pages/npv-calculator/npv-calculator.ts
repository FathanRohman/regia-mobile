import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: "page-npv-calculator",
  templateUrl: "npv-calculator.html"
})
export class NpvCalculatorPage {
  appType = "CASH";

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() { }

}
