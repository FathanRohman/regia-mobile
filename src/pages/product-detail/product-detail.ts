import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductUnitTypeListPage } from '../product-unit-type-list/product-unit-type-list';

@IonicPage()
@Component({
  selector: "page-product-detail",
  templateUrl: "product-detail.html"
})
export class ProductDetailPage {
  products: any = [
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      type: "Type 36/60",
      total: 240
    },
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      type: "Type 36/70",
      total: 100
    },
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      type: "Type 36/80",
      total: 200
    },
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      type: "Type 36/90",
      total: 10
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
      
    }

  ionViewDidLoad() {}

  detailProductUnitTypeLists () {
    this.navCtrl.push(ProductUnitTypeListPage)
  }
}
