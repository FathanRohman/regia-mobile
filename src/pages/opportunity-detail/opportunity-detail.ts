import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentMethodPage } from '../payment-method/payment-method';

@IonicPage()
@Component({
  selector: 'page-opportunity-detail',
  templateUrl: 'opportunity-detail.html',
})
export class OpportunityDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {}

  paymentMethodDetail () {
    this.navCtrl.push(PaymentMethodPage)
  }
}
