import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OpportunityNewPage } from '../opportunity-new/opportunity-new';
import { OpportunityDetailPage } from '../opportunity-detail/opportunity-detail';

@Component({
  selector: "page-opportunity",
  templateUrl: "opportunity.html"
})
export class OpportunityPage {
  constructor(
    public navCtrl: NavController
  ) {}

  toNewOpportunity () {
    this.navCtrl.push(OpportunityNewPage, {}, { animate: true })
  }

  toDetailOpportunity () {
    this.navCtrl.push(OpportunityDetailPage, {}, { animate: true })
  }
}
