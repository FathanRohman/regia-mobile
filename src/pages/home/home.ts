import { Component } from "@angular/core";
import { App, NavController } from "ionic-angular";
import { ProductPage } from '../product/product'
import { ProspectPage } from '../prospect/prospect';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tab2Root = ProspectPage

  constructor(
    public navCtrl: NavController,
    public appCtrl: App
  ) {

  }

  toProduct () {
    this.navCtrl.push(ProductPage, {}, { animate: true })
  }

  toProspect () {
    this.navCtrl.parent.select(1)
  }
}
