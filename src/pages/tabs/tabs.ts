import { Component } from '@angular/core';

import { ProspectPage } from '../prospect/prospect';
import { OpportunityPage } from '../opportunity/opportunity';
import { HomePage } from '../home/home';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProspectPage;
  tab3Root = OpportunityPage;
  tab4Root = MorePage;

  constructor() {

  }
}
