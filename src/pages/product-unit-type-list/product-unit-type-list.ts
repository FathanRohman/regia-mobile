import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductDetailUnitPage } from '../product-detail-unit/product-detail-unit';

@IonicPage()
@Component({
  selector: 'page-product-unit-type-list',
  templateUrl: 'product-unit-type-list.html',
})
export class ProductUnitTypeListPage {

  units: any = [
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      price: "1000000",
      position: 'Block C5'
    },
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      price: "2500000",
      position: 'Block D5'
    },
    {
      image: "https://gambar-rumah.com/athumb/e/1/3/big2568774.jpg",
      price: "500000",
      position: 'Block D10'
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
  }

  ionViewDidLoad() {}

  productDetailUnit () {
    this.navCtrl.push(ProductDetailUnitPage)
  }
}
