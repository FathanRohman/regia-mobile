import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationProvider {

  constructor(public http: HttpClient) {}

}
